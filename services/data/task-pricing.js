module.exports = {
    facebook: [
        {
          "id": 1,
          "name": "like",
          "success_mission_pricing": 250,
          "mission_unit_pricing": 350,
          "license_id": 1,
          "platform": "fb"
        },
        {
          "id": 2,
          "name": "comment",
          "success_mission_pricing": 560,
          "mission_unit_pricing": 660,
          "license_id": 1,
          "platform": "fb"
        },
        {
          "id": 3,
          "name": "share",
          "success_mission_pricing": 700,
          "mission_unit_pricing": 880,
          "license_id": 1,
          "platform": "fb"
        },
        {
          "id": 4,
          "name": "join_group",
          "success_mission_pricing": 1300,
          "mission_unit_pricing": 1500,
          "license_id": 1,
          "platform": "fb"
        },
        {
          "id": 5,
          "name": "follow",
          "success_mission_pricing": 560,
          "mission_unit_pricing": 660,
          "license_id": 1,
          "platform": "fb"
        },
        {
          "id": 6,
          "name": "reaction",
          "success_mission_pricing": 450,
          "mission_unit_pricing": 550,
          "license_id": 1,
          "platform": "fb"
        },
        {
          "id": 7,
          "name": "like_page",
          "success_mission_pricing": 700,
          "mission_unit_pricing": 800,
          "license_id": 1,
          "platform": "fb"
        }
      ],
    instagram: [
        {
          "id": 23,
          "name": "ins-like",
          "success_mission_pricing": 3000,
          "mission_unit_pricing": 500,
          "license_id": 1,
          "platform": "ins"
        },
        {
          "id": 26,
          "name": "ins-comment",
          "success_mission_pricing": 4000,
          "mission_unit_pricing": 1500,
          "license_id": 1,
          "platform": "ins"
        },
        {
          "id": 29,
          "name": "ins-follow",
          "success_mission_pricing": 5000,
          "mission_unit_pricing": 2500,
          "license_id": 1,
          "platform": "ins"
        }
      ]
}