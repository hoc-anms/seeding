const telegramLogger = require("../telegram/logger");
const axios = require("axios");
const https = require("https");
const token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQ3NTc5LCJlbWFpbCI6InBoYW1ob2MxOTk2QGdtYWlsLmNvbSIsInVzZXJJRCI6NDc1NzksImlzcyI6Imh0dHBzOlwvXC9hcHAuc2VlZGluZy52blwvc2VjdXJlXC9nZXQtdXNlci10b2tlbiIsImlhdCI6MTY1NjkxNzgzOSwiZXhwIjoxNjU3NTIyNjM5LCJuYmYiOjE2NTY5MTc4MzksImp0aSI6IjNMOHVQNXBXNzMya3pMRmoifQ.pmeFoPr_rK3odWjxm94DpBg7NEokKu02uHnrA6VFcdI";

const httpsAgent = new https.Agent({
  rejectUnauthorized: false, // (NOTE: this will disable client verification)
});

function delay(time) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
}

async function checkMission(typeTask, typePlatform) {
  try {
    let body =
      typePlatform === "facebook"
        ? {
            id_user: 47579,
            type_task: typeTask,
            client: "web",
            id_facebook: "100011555977158",
          }
        : {
            id_user: 47579,
            type_task: typeTask,
            client: "web",
            id_instagram: "30783957151",
            platform_seeding: "ins",
          };

    const resp = await axios({
      url: `https://app.seeding.vn:5428/get-count-mission`,
      method: "POST",
      json: true,
      headers: {
        Accept: "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.9",
        Connection: "keep-alive",
        "Content-Type": "application/json",
        Origin: "https://app.seeding.vn",
        Referer: "https://app.seeding.vn/",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-site",
        "User-Agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
        "sec-ch-ua":
          '".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"Linux"',
        token: token,
      },
      data: body,
      httpsAgent: httpsAgent,
    });

    console.log(resp.data);

    if (
      resp.data &&
      resp.data.status === "success" &&
      resp.data.mission_count > 0
    ) {
      await telegramLogger.info(
        `
                <b>Seeding (Check Mission)</b> <b>:</b>
                - <b>Platform: ${typePlatform}</b> 
                - <b>Task Type: ${typeTask}</b> 
                - <b>Quantity: ${resp.data.mission_count}</b>`
      );
    }
  } catch (e) {
    console.error(e);
  }
}

async function main() {
  try {
    const fbType = ["join_group"],
      instaType = ["ins-like", "ins-comment", "ins-follow"];

    for (let i = 0; i < fbType.length; i++) {
      await checkMission(fbType[i], "facebook");
    }

    for (let i = 0; i < instaType.length; i++) {
      await checkMission(instaType[i], "instagram");
      delay(500);
    }

    console.log("done checking!");
    process.exit(0);
  } catch (e) {
    console.error(e);
    try {
      await telegramLogger.error(
        `<b>Seeding (Check Mission)</b> <b>:</b> ${e.message ? e.message : e}`
      );
    } catch (e) {
      console.error(e);
    }
    process.exit(1);
  }
}

main();
