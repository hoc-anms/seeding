const { sendMessage } = require('./send-message');
const telegramConfig = require('../configs/telegram.cfg');

const maxDataLen = 1000;

function truncateData(data) {
    const d = `${data}`;
    if (d.length > maxDataLen) {
        return d.substring(0, 1000) + '...';
    }

    return d;
}

async function log(data) {
    if (telegramConfig.enableLogger) {
        await sendMessage({
            chatId: telegramConfig.appBot.groupId,
            text: `<b>[LOG] </b>${truncateData(data)}`,
            parseMode: 'HTML',
        });
    }
}

async function info(data) {
    if (telegramConfig.enableLogger) {
        await sendMessage({
            chatId: telegramConfig.appBot.groupId,
            text: `<b>[INFO] </b>${truncateData(data)}`,
            parseMode: 'HTML',
        });
    }
}

async function error(data) {
    if (telegramConfig.enableLogger) {
        await sendMessage({
            chatId: telegramConfig.appBot.groupId,
            text: `<b>[ERROR] </b>${truncateData(data)}`,
            parseMode: 'HTML',
        });
    }
}

async function send(data) {
    if (telegramConfig.enableLogger) {
        await sendMessage({
            ...data,
            chatId: telegramConfig.appBot.groupId,
        });
    }
}

module.exports = {
    log,
    info,
    error,
    send,
};
