const axios = require('axios');
const telegramConfig = require('../configs/telegram.cfg');

async function sendMessage(params) {
    const {
        chatId,
        text,
        parseMode,
        disableWebPagePreview,
        disableNotification,
        replyToMessageId,
        replyMarkup,
    } = params;

    const resp = await axios({
        url: `https://api.telegram.org/bot${telegramConfig.appBot.token}/sendMessage`,
        method: 'POST',
        json: true,
        data: {
            chat_id: chatId,
            text,
            parse_mode: parseMode,
            disable_web_page_preview: disableWebPagePreview,
            disable_notification: disableNotification,
            reply_to_message_id: replyToMessageId,
            reply_markup: replyMarkup,
        },
    });

    return resp;
}

module.exports = {
    sendMessage,
};
